package br.com.mastertech.controller;

import br.com.mastertech.dtos.ClienteMapper;
import br.com.mastertech.dtos.CreateCadastroRequest;
import br.com.mastertech.model.Cliente;
import br.com.mastertech.producer.Cadastro;
import br.com.mastertech.service.CadastroService;
import br.com.mastertech.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private ClienteMapper clienteMapper;

    @Autowired
    private CadastroService cadastroService;

    @PostMapping
    public Cliente createCadastro(@RequestBody @Valid CreateCadastroRequest createCadastroRequest){
        Cliente cliente = clienteMapper.toCliente(createCadastroRequest);
        Cadastro cadastro = clienteMapper.toCadastro(cliente);

        Cliente response = clienteService.create(cliente);
        cadastroService.sendToKafka(cadastro);

        return response;
    }
}
