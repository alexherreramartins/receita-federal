package br.com.mastertech.dtos;

import br.com.mastertech.model.Cliente;
import br.com.mastertech.producer.Cadastro;
import org.springframework.stereotype.Component;

@Component
public class ClienteMapper {

    public Cliente toCliente(CreateCadastroRequest createCadastroRequest){

        Cliente cliente = new Cliente();

        cliente.setCnpj(createCadastroRequest.getCnpj());
        cliente.setName(createCadastroRequest.getNomeEmpresa());

        return cliente;
    }

    public Cadastro toCadastro(Cliente cliente) {
        Cadastro cadastro = new Cadastro();

        cadastro.setCnpj(cliente.getCnpj());
        cadastro.setNome(cliente.getName());

        return cadastro;
    }
}
