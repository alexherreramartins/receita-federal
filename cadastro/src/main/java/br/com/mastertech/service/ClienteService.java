package br.com.mastertech.service;

import br.com.mastertech.model.Cliente;
import br.com.mastertech.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente create(Cliente cliente) {
        cliente.getCnpj().replace(".","").replace("/","");
        return clienteRepository.save(cliente);
    }
}
