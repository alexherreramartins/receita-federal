package br.com.mastertech.service;

import br.com.mastertech.producer.Cadastro;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class CadastroService {

    @Autowired
    private KafkaTemplate<String, Cadastro> producer;

    public void sendToKafka(Cadastro cadastro) {
        producer.send("spec4-alex-herrera-2", cadastro);
    }
}
